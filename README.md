# To buid and run locally the project after clone the project

First make sure the Saleor API is running, then:

`cd vue-store`

`npm install`

`npm run dev`

# Commands step by step used to set up this project

```
nvm use 18.13.0

npm create vite@latest

//options -> Project name: vue-store; vue; javascript

cd vue-store

npm install

npm install --save graphql graphql-tag @apollo/client

npm install --save @vue/apollo-composable

npm run dev

```

# How it looks like

![](vuestore.png)

# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).
