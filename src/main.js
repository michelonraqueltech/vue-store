import { createApp, h, provide, render } from 'vue'
import './style.css'
import App from './App.vue'

import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'

import { DefaultApolloClient } from '@vue/apollo-composable'

// HTTP connection to the API
const httpLink = createHttpLink({
  // You should use an absolute URL here
  uri: 'http://localhost:8000/graphql/',
})

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
  connectToDevTools: true,
  link: httpLink,
  cache,
  // Enable sending cookies over cross-origin requests
  credentials: 'include',
//   headers: {
//     authorization: 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjEiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE2NzgxMjA5NDAsIm93bmVyIjoic2FsZW9yIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDAwL2dyYXBocWwvIiwiZXhwIjoxNjc4MTIxMjQwLCJ0b2tlbiI6ImNHOW1OOTJ0YWhZMiIsImVtYWlsIjoiYWRtaW5AZXhhbXBsZS5jb20iLCJ0eXBlIjoiYWNjZXNzIiwidXNlcl9pZCI6IlZYTmxjam94IiwiaXNfc3RhZmYiOnRydWV9.gbEQCb8fOR-U5-xek99wy96nGN1u3Je1mILsfHQ-J6heOzAzoiL8uwHdpxVQUQwghLyKrhXr2AE0LzvwfEhn4aeuROyq-pRMCYAIBfq1cOsLIweKIouLnMKocFfSq41PagFaYAxXxr-bYn8FlKXnJUKNory2tZkONIWzZ-adJwt4kXM_CL4VOOH5c27fhoTM57Xm_zVytL131-OinsQMaQzLAxs906eDFJd-xoUljxlTwgmZDhB0M4FZObPO6l89NR_i3a8Sbsjru9LtNpU0LFQMTDxsEU0KI0vB1fGl9T6bIammVhKlQqKmRsql9RD2itxzGGazBthoI3s4_JB4dw',
//     authorization: localStorage.getItem('token'),
//   }
})

const app = createApp({
    setup() {
        provide(DefaultApolloClient, apolloClient)
    },
    render: () => h(App)
})

app.mount('#app'); 
